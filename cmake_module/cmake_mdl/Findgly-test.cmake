# -----------------------------------------------------------------------------
#  Cmake File for module add
#  File Name      :  Findgly-test.cmake
#  CMake Version  :  ^3.17
#  Author         :  Shibo Jiang  
#  Instructions   :  Initial file.             2022/7/27                 V0.1
# 
# -----------------------------------------------------------------------------
# 辅助输出信息
message("now using Findgly-test.cmake find gly-test lib")

# gly_test.h 文件路径赋值给 GLYTEST_LIB_INCLUDE_DIR
FIND_PATH(GLYTEST_LIB_INCLUDE_DIR 
        gly_test.h 
        /usr/local/gly/gly-test/include/)
message(".h dir ${GLYTEST_LIB_INCLUDE_DIR}")

# 将 libglytest_lib.a 文件路径赋值给 GLYTEST_LIB_LIBRARY
FIND_LIBRARY(GLYTEST_LIB_LIBRARY 
            libglytest_lib.a 
            /usr/local/gly/gly-test/lib/)
message("lib dir: ${GLYTEST_LIB_LIBRARY}")

if(GLYTEST_LIB_INCLUDE_DIR AND GLYTEST_LIB_LIBRARY)
    # 设置变量结果
    set(GLYTEST_FOUND TRUE)
endif()