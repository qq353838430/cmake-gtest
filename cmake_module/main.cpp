/**
 *-----------------------------------------------------------------------------
 * @file main.cpp
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2022-07-31
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#include "gly_test.h"

int main(int argc, char* argv[])
{
    gly::print_gly();
    return 0;
}