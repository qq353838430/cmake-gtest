/**
 *-----------------------------------------------------------------------------
 * @file gly_test.cpp
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2022-07-31
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#include "gly_test.h"
#include <iostream>

namespace gly
{
void print_gly()
{
    std::cout<<"this is cmake module demo"<<std::endl;
}
}