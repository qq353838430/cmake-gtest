/**
 *-----------------------------------------------------------------------------
 * @file gly_test.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2022-07-31
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#ifndef GLY_TEST_H
#define GLY_TEST_H

namespace gly
{
void print_gly();
}


#endif