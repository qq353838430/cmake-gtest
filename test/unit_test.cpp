/*---------------------------------------------------------------------------*/
/*   Sample code for unit test , using google test          
/*   File Name    :  getst.cpp
/*   Complier     :  C++ 11
/*   Author       :  Shibo Jiang     
/*   Instructions :  Initial file.             2020/5/27              V0.1
/*          
/*---------------------------------------------------------------------------*/

/* Include Files */
#include <vector>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "mock_func.h"

extern "C" {
    #include "sample_func.h"
    #include "list_node.h"
    #include "led_ctl.h"
    #include "can_ctl.h"
}

using namespace testing;

/**
 *-----------------------------------------------------------------------------
 * @brief 使用测试夹具自动创建和销毁仿制函数对象
 * 
 *-----------------------------------------------------------------------------
 */
class GlobalEnvironment : public testing::Environment
{
public:
    virtual void SetUp()
    {
        g_gpioMock = new CGpioMock();
        g_serialMock = new CSerialMock();
    }
    virtual void TearDown()
    {
        delete g_gpioMock;
        delete g_serialMock;
    }
};

/* System Function  ---------------------------------------------------------*/
int main(int argc, char **argv)
{
    testing::AddGlobalTestEnvironment(new GlobalEnvironment ());
    testing::InitGoogleTest(&argc, argv);
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

static bool ArrayCompare(const int* exp, const int* act, int len) 
{
    bool flag = true;
    for (int i = 0; i < len; i++)
    {
        if (exp[i] != act[i])
        {
            flag = false;
            break;
        }
    }
    return flag; 
}
/* Function end--------------------------------------------------------------*/

/* ######################## Test Case Start #################################*/

/* Test Suit IntSwapTest-----------------------------------------------------*/
TEST(IntSwapTest, swapSuccess)
{
    int a = 1;
    int b = 2; 
    int_swap(&a, &b); 
    EXPECT_EQ(2, a) << "Expect a is 2";
    EXPECT_EQ(1, b);
}

TEST(IntSwapTest, paramError)
{
    int a = 1; 
    int_swap(&a, NULL); 
    EXPECT_EQ(1, a);
    int_swap(NULL, &a); 
    EXPECT_EQ(1, a);
}
/* Function end--------------------------------------------------------------*/

/* Test Suit SqrtTest--------------------------------------------------------*/
TEST(SqrtTest, normalTest) 
{
    EXPECT_NEAR(1e-50, sqrt(1e-100), 1e-12);
    EXPECT_NEAR(1, sqrt(1), 1e-12);
    EXPECT_NEAR(1e50, sqrt(1e100), 1e-12);
}
/* Function end--------------------------------------------------------------*/

/* Test Suit ByteSwapTest----------------------------------------------------*/
TEST(ByteSwapTest, stringSwap)
{
    char str1[10] = "123456789";
    char str2[10] = "abcdefghi"; 
    byte_swap(str1, str2, 5); 
    EXPECT_STREQ("abcde6789", str1) << "Expect str1 is abcde6789"; 
    EXPECT_STREQ("12345fghi", str2) << "Expect str2 is 12345fghi"; 
}

TEST(ByteSwapTest, paramError)
{
    char str1[10] = "123456789";
    char str2[10] = "abcdefghi"; 
    byte_swap(str1, NULL, 5);
    byte_swap(NULL, str2, 5);
    byte_swap(str1, str2, 0); 
    EXPECT_STREQ("123456789", str1);
    EXPECT_STREQ("abcdefghi", str2);
}
/* Function end--------------------------------------------------------------*/

/* Test Suit BubblSortTest---------------------------------------------------*/
TEST(BubblSortTest, normalTest) 
{
    int expArray[5] = { 1, 2, 3, 4, 5 };
    int actArray[5] = { 3, 1, 5, 4, 2 };
    bubblesort(actArray, 5);
    EXPECT_PRED3(ArrayCompare, expArray, actArray, 5);
}
/* Function end--------------------------------------------------------------*/

/* Test Suit list_node-------------------------------------------------------*/
TEST(list_node, normalTest) 
{
    int expArray[5] = { 5, 4, 3, 2, 1 };
    int actArray[5] = { 1, 2, 3, 4, 5 };
    
    list_single *node = create_list_node(5);
    node->next = create_list_node(4);
    node->next->next = create_list_node(3);
    node->next->next->next = create_list_node(2);
    node->next->next->next->next = create_list_node(1);

    list_node *tp_node = node;
    for (size_t i = 0; i < 5; i++)
    {
        actArray[i] = tp_node->data;
        tp_node = tp_node->next;
    }
    EXPECT_PRED3(ArrayCompare, expArray, actArray, 5);
    free(node);
}
/* Function end--------------------------------------------------------------*/

/* Test Suit led_control ----------------------------------------------------*/
/**
 *-----------------------------------------------------------------------------
 * @brief Construct a new TEST object
 * 定义了 4 个 led 灯的相关参数，并调用 led_init 对 4 个 led 灯进行初始化，
 * 其中，*g_gpioMock 的 gpio_pin_cfg 函数期望被调用 4 次，
 * 由于每次调用希望的参数不同，所以定义了 4 个预期调用。
 * 在每个测试用例的最后，一定要调用 
 * testing::Mock::VerifyAndClearExpectations(void * mock_obj) 函数验证使用到的
 * 所有的仿制对象，以免影响到其他用例的执行。
 * 可以使用.After(expectations)指定一个或多个期望调用expectations，
 * 表示期望当前定义的期望调用将在 expectations 指定的期望调用之后匹配
 * 
 * testing::Expectation exp = EXPECT_CALL(mock_object, method(matchers))
                            .With(double_argument_matcher)
                            .With(double_argument_matcher);

 * - 宏 EXPECT_CALL 的返回值为 testing::Expectation 类型，可以使用一个变量保存起来；

    - mock_object 是期望调用的仿制对象实例的名称；

    - method 是期望调用的仿制对象的函数的名称；

    - matchers 是多个单参数匹配器，每个参数有仅只有一个单参数匹配器，
        用来指定仿制对象函数被调用时传入的各个参数需要满足的条件；

    - With(double_argument_matcher)用来指定一个双参数匹配器，
        可以选定两个参数并描述两个参数应满足的关系，
        可以多次使用以描述多对参数应满足的关系。
 * 
 *-----------------------------------------------------------------------------
 */
TEST(LedTest, initOk)
{
    LedInfoType g_led_info[] = { { 5, true }, { 4, true }, { 3, false }, { 2, false } };
    uint32_t g_led_num = sizeof(g_led_info) / sizeof(g_led_info[0]);
    /* 定义一个 testing::InSequence 类型的队列 seq，
       然后在 seq 的作用域内定义的所有期望调都将自动加入到队列 seq 中,此功能可省略 .After
       凡加入到队列中的期望调用都会被认为使用了.RetiresOnSaturation() ,会自动进行
       调用顺序判定
    */
    testing::InSequence seq;

    testing::Expectation ec5h, ec4h, ec3l, ec2l;
    // 调用函数次数及顺序判断，是否为期望调用逻辑
    ec5h = EXPECT_CALL(*g_gpioMock, gpio_pin_cfg(5, GPIO_OUTPUT_INIT_HIGH));
    ec4h = EXPECT_CALL(*g_gpioMock, gpio_pin_cfg(4, GPIO_OUTPUT_INIT_HIGH)).After(ec5h);
    ec3l = EXPECT_CALL(*g_gpioMock, gpio_pin_cfg(3, GPIO_OUTPUT_INIT_LOW)).After(ec4h);
    ec2l = EXPECT_CALL(*g_gpioMock, gpio_pin_cfg(2, GPIO_OUTPUT_INIT_LOW)).After(ec3l);

    led_init(g_led_info, g_led_num);
    testing::Mock::VerifyAndClearExpectations(g_gpioMock);
}

TEST(LedTest, initOk_withAction)
{
    LedInfoType g_led_info[] = { { 5, true }, { 4, true }, { 3, false }, { 2, false } };
    uint32_t g_led_num = sizeof(g_led_info) / sizeof(g_led_info[0]);
    // 调用函数次数判断，并同时设定调用函数具备返回值
    /* 第 1 个.WillOnce(Return(1))表示在第 1 次匹配成功时返回 1，
       第 2 个.WillOnce(Return(1))表示第 2 次匹配成功时返回 1，
       .WillRepeatedly(Return(1))表示后续匹配成功时均返回 1。
       需要注意的是，当使用了.WillRepeatedly(action)后不再可以使用.WillOnce(action) 
       或.WillRepeatedly(action)来指定其他行为。
    */
    EXPECT_CALL(*g_gpioMock, gpio_pin_cfg(_, _))
                .Times(4)
                .WillOnce(Return(1))
                .WillOnce(Return(1))
                .WillRepeatedly(Return(1));

    led_init(g_led_info, g_led_num);
    testing::Mock::VerifyAndClearExpectations(g_gpioMock);
}


/* Test Suit CAN_control ----------------------------------------------------*/
TEST(CANbusTest, sendOk)
{
    char cmd[20] = ":010100000008F6\r\n";
    int len = strlen(cmd);
    // 调用函数入参判断，是否为期望入参
    EXPECT_CALL(*g_serialMock, SerialWrite(StrEq(":F10100000008F6\r\n"), len, NotNull()));
    CANbusSend(cmd, len);
    testing::Mock::VerifyAndClearExpectations(g_serialMock);
}

bool MyMatcher(char* arg)
{
    int len = strlen(arg);
    return ((':' == arg[0]) && ('\r' == arg[len - 2]) && ('\n' == arg[len - 1]));
}

TEST(CANbusTest, sendOk_withUserDefMatcher)
{
    char cmd[20] = ":010100000008FF\r\n";
    int len = strlen(cmd);
    // 使用自定义匹配器，进行函数入参判断
    EXPECT_CALL(*g_serialMock, SerialWrite(ResultOf(MyMatcher, true), len, NotNull()));
    CANbusSend(cmd, len);
    testing::Mock::VerifyAndClearExpectations(g_serialMock);
}

/**
 *-----------------------------------------------------------------------------
 * @brief Construct a new TEST object
 * 
 * SetArrayArgument<N> (first,last)将一段数据存储到第 N 个变量指向的内存中，
 * 第 N 个变量应为指针类型或数组类型，first 为起始地址，last 为结束地址
 * 
 * Ge(value) 传入的值不能小于 value
 * 
 * NotNull()针对指针参数，参数不能为空指针
 * 
 *-----------------------------------------------------------------------------
 */
#if 0

TEST(CANbusTest, readOk)
{
    char data[20] = ":0101015AA3\r\n";
    char buff[20] = { 0 };
    int nByteRead = 0;
    // 针对使用函数入参当作功能返回值的情况，进行函数入参值得设定
    EXPECT_CALL(*g_serialMock, SerialRead(NotNull(), Ge(20), NotNull()))
        .WillOnce(SetArrayArgument<0>(data, data + strlen(data)));
    CANbusRead(buff, 20, &nByteRead);
    testing::Mock::VerifyAndClearExpectations(g_serialMock);
}
#endif

/**
 *-----------------------------------------------------------------------------
 * @brief Set the Param And Return object
 *        方便测试的桩参数修改函数，需要在测试框架调用到 SerialRead 函数时，
 *        同时调用此函数进行相关参数修改
 * 
 * @param buff 
 * @param buffLen 
 * @param nbyteRead 
 * @return true 
 * @return false 
 *-----------------------------------------------------------------------------
 */
bool SetParamAndReturn(char* buff, int buffLen, int* nbyteRead)
{
    char test_array[] = ":0101015AA3\r\n";
    strcpy(buff, test_array);
    *nbyteRead = strlen(test_array);
    return true;
}

/**
 *-----------------------------------------------------------------------------
 * @brief Construct a new TEST object
 * 在测试 CANbus 接收数据时，需要同时向 buff 中填入数据、填入nbyteRead 
 * 的值并返回一个值，在这种情况下可以用一个函数来完成这三个动作，
 * 并使用 Invoke(f)调用这个函数以完成这三个动作
 * 
 * Invoke(f)使用传入的各个参数调用另一个函数 f，并使用 f 的返回值返回。
 * 要求 f 原型和正在匹配的仿制对象函数原型相同。
 * 
 *-----------------------------------------------------------------------------
 */
TEST(CANbusTest, readOk_withInvokeFunc)
{
    char buff[20] = { 0 };
    int nByteRead = 0;

    EXPECT_CALL(*g_serialMock, SerialRead(NotNull(), Ge(20), NotNull()))
            .WillOnce(Invoke(SetParamAndReturn));
    CANbusRead(buff, 20, &nByteRead);
    testing::Mock::VerifyAndClearExpectations(g_serialMock);
}