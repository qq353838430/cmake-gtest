/**
 *-----------------------------------------------------------------------------
 * @file mock_func.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#ifndef _mock_func_h_
#define _mock_func_h_

#include "gmock/gmock.h"

/**
 *-----------------------------------------------------------------------------
 * @brief 定义了一个仿制对象类 CGpioMock，并在类 CGpioMock 中生成了控制gpio 的
 * 3 个基本的函数（这里只生成了测 led 代码所需要的函数，并没有包含控制 gpio 
 * 的所有函数）。gmock 会自动生成函数体。
 * 
 * MOCK_METHODX(func_name, return_type(arg_list))
 * 
 * X 代表生成的函数的参数个数，
 * func_name 是函数名称，
 * return_type 函数的返回值类型，
 * arg_lsit 是函数的参数列表，
 * X 最大为 10，也就是说可以使用这个宏生成 0\~10 个参数的函数。
 * 
 *-----------------------------------------------------------------------------
 */
class CGpioMock
{
public:
    MOCK_METHOD2(gpio_pin_cfg, int(int pin, uint32_t flags));
    MOCK_METHOD1(gpio_get, int(int pin));
    MOCK_METHOD2(gpio_set, int(int pin, int value));
};

/**
 *-----------------------------------------------------------------------------
 * @brief 仿制串口通讯函数
 * 
 *-----------------------------------------------------------------------------
 */
class CSerialMock
{
public:
    MOCK_METHOD3(SerialWrite, bool(char* data, int len, int* nbyteWrite));
    MOCK_METHOD3(SerialRead, bool(char* buff, int buffLen, int* nbyteRead));
};

// Extern Parameter
extern CGpioMock *g_gpioMock;
extern CSerialMock *g_serialMock;

#endif