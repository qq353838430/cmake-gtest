/**
 *-----------------------------------------------------------------------------
 * @file test_type.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#ifndef _TEST_TYPE_H_
#define _TEST_TYPE_H_


// Used Macro
#define GPIO_OUTPUT_INIT_HIGH 1
#define GPIO_OUTPUT_INIT_LOW 0






#endif