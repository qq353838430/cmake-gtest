/**
 *-----------------------------------------------------------------------------
 * @file mock_func.cpp
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#include "mock_func.h"

CGpioMock *g_gpioMock= NULL;
CSerialMock *g_serialMock = NULL;

// 由于被测对象为 c语言，所以仿制函数需要以c的方式去编译
extern "C" {    
// GPIO func mock
int gpio_pin_cfg(int pin, uint32_t flags)
{
    return g_gpioMock->gpio_pin_cfg(pin, flags);
}

int gpio_get(int pin)
{
    return g_gpioMock->gpio_get(pin);
}

int gpio_set(int pin, int value)
{
    return g_gpioMock->gpio_set(pin, value);
}

// Serial func mock
bool SerialWrite(void* data, int len, int* nbyteWrite)
{
    return g_serialMock->SerialWrite((char*)data, len, nbyteWrite);
}

bool SerialRead(void* buff, int buffLen, int* nbyteRead)
{
    return g_serialMock->SerialRead((char*)buff, buffLen, nbyteRead);
}
}