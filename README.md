CMake-GTest
===========

# 1. 介绍

用于演示基于CMake 调用GTest进行单元测试，如下示例均基于Ubuntu 22.04 lts 系统，windows 系统需要自行安装  git，cmake ，TDM-GCC ，ninja 这四款软件，并将相应路径添加到环境变量中。

# 2. 环境准备

首先需要安装本工程所需的软件，需要安装git，c语言编译器，cmake 构建器 ，以及 ninja 生成器

`sudo apt install git gcc g++ cmake ninja-build`

## 2.1. git 生成 ssh 密钥，添加到gitee中

- `ssh-keygen -t ed25519`
- 生成的 pub key 粘贴到gitee中 ，此处参考 [使用Gitee - 廖雪峰的官方网站 (liaoxuefeng.com)](https://www.liaoxuefeng.com/wiki/896043488029600/1163625339727712)

## 2.2. 环境检测

- 检测 gcc 是否安装成功

```
$ gcc --version
gcc (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

- 检测 ninja 是否安装成功

$ ninja --version
1.10.1

- 检测 Cmake 是否安装成功

```
$ cmake --version
cmake version 3.22.1

CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

## 2.3. gtest样例工程概要

提供的样例工程目录结构如下

![1676172970681.png](https://img.jiangshibo.cn/i/2023/02/12/1676172970681.png)

cmake 构建系统结构介绍:

![gtest_cmake_struct.png](./docs/gtest_cmake_struct.png)

## 2.4. 编译gtest样例工程

使用CMake进行构建编译，就是经典的四步，进入cmake文件夹后，新建build构建目录，使用 `cmake ..`命令，使用build上一层目录中CMakeLists.txt 文件进行cmake工程创建和处理，使用 `cmake --build .` 命令，调用设定好的生成器进行目标构建。

```
cd cmake
mkdir build
cd build
cmake ..
cmake --build .
```

- Git clone [CMake-GTest](https://gitee.com/qq353838430/cmake-gtest) ，检出master分支，并进入 `cmake-gtest\cmake`目录
- 在test文件夹中新建 build 文件夹 ，作为构建地址

  ![gtest_env_004](./docs/gtest_env_004.png)
- 进入 build目录下 ，依次执行 `cmake ..` , `cmake --build .`

  ![cmake_use](./docs/cmake使用.gif)
  ![img](./docs/cmake-build.gif)

## 2.5. 查看单元测试执行情况

- 在 build 目录下 ，执行 `ctest`
  ![1676174109288.gif](https://img.jiangshibo.cn/i/2023/02/12/1676174109288.gif)

# 3. Cmake Ninja 使用技巧

## 3.1. 加速编译 - 调用多核CPU并行运算

Cmake 支持在调用时使用管道技术将命令传入 Ninja 中，ninja工具支持调用多核CPU进行并行编译，以此来加快编译速度。

- 使用 ninja 自带工具清空编译文件, `ninja -t clean`
- 使用并行运算程编译命令，开启6个线程进行并行编译，`cmake --build . -j 6`

## 3.2. 构建依赖关系图生成

Ninja 带有构建依赖关系生成工具，可以生成 graphviz 格式(*.dot)的图形，若环境中已经安装 graphviz 工具，可以直接使用管道调用工具渲染成 svg 矢量图。

- 检查环境是否安装 `dot -V`
- 使用管道将 `ninja -t graph` 输出作为入参，调用 `dot -T svg -o test.svg` ,生成svg文件

  `ninja -t graph|dot -T svg -o test.svg`

  ![graphviz](./docs/依赖关系部分预览.png)
