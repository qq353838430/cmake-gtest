/**
 *-----------------------------------------------------------------------------
 * @file sample_func.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#ifndef _SAMPLE_FUNC_H
#define _SAMPLE_FUNC_H
/* Include Files */
#include "Rte_Type.h"
#include "sample_func1.h"
#include "sample_func2.h"

/* Function Declation */
extern void int_swap(int* p1, int* p2);
extern double sqrt(double val);

#endif
