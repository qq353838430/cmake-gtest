/**
 *-----------------------------------------------------------------------------
 * @file led_ctl.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#ifndef _LED_CTL_H_
#define _LED_CTL_H_

#include "Rte_Type.h"

// Data type define
typedef struct am_led_info 
{
    uint32  pin;        /* LED 占用的 GPIO 引脚 */
    boolean active_low; /* LED 亮时引脚是否低电平 */
} LedInfoType;

// Led Interface
extern void led_init(const LedInfoType *p_led_info, const uint32 led_num); /* LED 初始化 */
extern void led_set(uint8 led_id, boolean state); /* 设置 LED 状态 */
extern void led_on(uint8 led_id); /* 点亮 LED */
extern void led_off(uint8 led_id); /* 关闭 LED */
extern boolean led_get(uint8 led_id); /* 获取 LED 状态 */

// Gipo Interface
extern int gpio_pin_cfg(int pin, uint32 flags); /* gpio 初始化 */
extern int gpio_get(int pin); /* 获取 gpio 状态 */
extern int gpio_set(int pin, int value); /* 设置 gpio 状态 */


#endif