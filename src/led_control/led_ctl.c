/**
 *-----------------------------------------------------------------------------
 * @file led_ctl.c
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#include "led_ctl.h"

LedInfoType *__pg_led_info = NULL;
uint32 __g_led_num = 0;

/**
 *-----------------------------------------------------------------------------
 * @brief LED 初始化
 * 
 * @param p_led_info 
 * @param led_num 
 *-----------------------------------------------------------------------------
 */
void led_init (const LedInfoType *p_led_info, const uint32 led_num)
{
    int i;
    if ((p_led_info != NULL) && (led_num != 0)) 
    {
        __pg_led_info = (LedInfoType *)p_led_info;
        __g_led_num = (uint32)led_num;
    } 
    for (i = 0; i < led_num; i++) 
    {
        if (p_led_info[i].active_low) 
        {
            gpio_pin_cfg(p_led_info[i].pin, GPIO_OUTPUT_INIT_HIGH);
        } 
        else 
        {
            gpio_pin_cfg(p_led_info[i].pin, GPIO_OUTPUT_INIT_LOW);
        }
    }
}

/**
 *-----------------------------------------------------------------------------
 * @brief LED 控制
 * 
 * @param led_id 
 * @param state 
 *-----------------------------------------------------------------------------
 */
void led_set (uint8 led_id, boolean state)
{
    if (led_id < __g_led_num) 
    {
        state = (boolean)(state ^ (__pg_led_info[led_id].active_low));
        gpio_set(__pg_led_info[led_id].pin, (int)state);
    }
}

/**
 *-----------------------------------------------------------------------------
 * @brief LED 开启
 * 
 * @param led_id 
 *-----------------------------------------------------------------------------
 */
void led_on (uint8 led_id)
{
    if (led_id < __g_led_num) 
    {
        if (__pg_led_info[led_id].active_low == true) 
        {
            gpio_set(__pg_led_info[led_id].pin, (int)false);
        } 
        else 
        {
            gpio_set(__pg_led_info[led_id].pin, (int)true);
        }
    } 
}

/**
 *-----------------------------------------------------------------------------
 * @brief LED 关闭
 * 
 * @param led_id 
 *-----------------------------------------------------------------------------
 */
void led_off (uint8 led_id)
{
    if (led_id < __g_led_num) 
    {
        if (__pg_led_info[led_id].active_low == true) 
        {
            gpio_set(__pg_led_info[led_id].pin, (int)true); 
        } 
        else 
        {
            gpio_set(__pg_led_info[led_id].pin, (int)false);
        }
    } 
}

/**
 *-----------------------------------------------------------------------------
 * @brief LED 状态获取
 * 
 * @param led_id 
 * @return boolean 
 *-----------------------------------------------------------------------------
 */
boolean led_get (uint8 led_id)
{
    boolean state;
    if (led_id < __g_led_num) 
    {
        state = (boolean)__pg_led_info[led_id].active_low;
        state ^= (boolean)gpio_get(__pg_led_info[led_id].pin);
        return state;
    }
    return false;
}