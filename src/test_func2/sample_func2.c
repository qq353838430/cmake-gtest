/**
 *-----------------------------------------------------------------------------
 * @file sample_func2.c
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

/* Include Files */
#include "sample_func2.h"

/* Function Declation */
void bubblesort(int* data, int len);

/* Function start -----------------------------------------------------------*/
void bubblesort(int* data, int len) 
{
    int temp = 0;
    for (int i = 0; i < len; i++)
    {
        for (int j = i + 1; j < len; j++)
        {
            if (data[i] > data[j])
            {
                temp = data[i]; 
                data[i] = data[j];
                data[j] = temp;
            }
        }
    }
}
/* Function end--------------------------------------------------------------*/