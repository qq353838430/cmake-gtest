/**
 *-----------------------------------------------------------------------------
 * @file sample_func2.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#ifndef _SAMPLE_FUNC2_H
#define _SAMPLE_FUNC2_H
/* Include Files */
#include "Rte_Type.h"

/* Function Declation */
extern void bubblesort(int* data, int len);

#endif
