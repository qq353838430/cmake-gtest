/**
 *-----------------------------------------------------------------------------
 * @file sample_func1.c
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

/* Include Files */
#include "sample_func1.h"

/* Function Declation */
void byte_swap(void *pData1, void *pData2, size_t stSize);

/* Function start -----------------------------------------------------------*/
void byte_swap(void *pData1, void *pData2, size_t stSize)
{
    if (pData1 && pData2 && stSize > 0)
    {
        unsigned char *pcData1 = (unsigned char*)pData1; 
        unsigned char *pcData2 = (unsigned char*)pData2; 
        unsigned char ucTemp;

        while(stSize--)
        {
            ucTemp = *pcData1; 
            *pcData1 = *pcData2; 
            *pcData2 = ucTemp; 
            pcData1++; 
            pcData2++;
        }
    }
}
/* Function end--------------------------------------------------------------*/