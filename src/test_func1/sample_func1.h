/**
 *-----------------------------------------------------------------------------
 * @file sample_func1.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */
#ifndef _SAMPLE_FUNC1_H
#define _SAMPLE_FUNC1_H
/* Include Files */
#include "Rte_Type.h"

/* Function Declation */
extern void byte_swap(void *pData1, void *pData2, size_t stSize);

#endif
