/**
 *-----------------------------------------------------------------------------
 * @file sample_func.c
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

/* Include Files */
#include "sample_func.h"

/* Function Declation */
void int_swap(int* p1, int* p2);
double sqrt(double val);

/* Function start -----------------------------------------------------------*/
void int_swap(int* p1, int* p2)
{
    if (p1 && p2)
    {
        int temp;
        temp = *p1;
        *p1 = *p2;
        *p2 = temp;
    }
}
/* Function end--------------------------------------------------------------*/

/* Function start -----------------------------------------------------------*/
double sqrt(double val) 
{
    if (val < 1e-100 || val > 1e100)
    {
        return 1;
    }
    double x1 = val / 2;
    double x2 = (x1 + val / x1) / 2;
    double err = x1 > x2 ? (x1 - x2) : (x2 - x1);
    for (int i = 0; i < 1000 && (err >= 1e-12 || err / x2 >= 1e-12); i++)
    {
        x1 = x2;
        x2 = (x1 + val / x1) / 2;
        err = x1 > x2 ? (x1 - x2) : (x2 - x1); 
    }
    return x2;
}
/* Function end--------------------------------------------------------------*/