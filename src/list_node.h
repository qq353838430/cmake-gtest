/**
 *-----------------------------------------------------------------------------
 * @file list_node.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#ifndef _LIST_NODE_H_
#define _LIST_NODE_H_

#include "Rte_Type.h"

/* Type define */
struct  list_node
{
    int data ; 
    struct list_node *next ;
};
typedef struct list_node list_single ; 

/* Function Declation */
extern list_single *create_list_node(int data);

#endif