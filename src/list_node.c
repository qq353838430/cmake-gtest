/**
 *-----------------------------------------------------------------------------
 * @file list_node.c
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

/* Include Files */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list_node.h"

/* Function start -----------------------------------------------------------*/
list_single *create_list_node(int data)
{
    list_single *node = NULL ;
    node = (list_single *)malloc(sizeof(list_single));
    if(node == NULL){
        printf("malloc fair!\n");
    }
    memset(node,0,sizeof(list_single));
    node->data = data;
    node->next = NULL ;
    return node ;
}
/* Function end--------------------------------------------------------------*/