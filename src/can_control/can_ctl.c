/**
 *-----------------------------------------------------------------------------
 * @file can_ctl.c
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#include "can_ctl.h"

/**
 *-----------------------------------------------------------------------------
 * @brief CAN 总线发送
 * 
 * @param cmd 
 * @param cmdLen 
 * @return boolean 
 *-----------------------------------------------------------------------------
 */
boolean CANbusSend(char* cmd, int cmdLen)
{ 
    int nbyteWrite = 0;
    return SerialWrite(cmd, cmdLen, &nbyteWrite);
}

/**
 *-----------------------------------------------------------------------------
 * @brief CAN 总线读取
 * 
 * @param revBuff 
 * @param buffLen 
 * @param nbyteRead 
 * @return boolean 
 *-----------------------------------------------------------------------------
 */
boolean CANbusRead(char* revBuff, int buffLen, int* nbyteRead)
{
    while (SerialRead(revBuff, buffLen, nbyteRead) && (*nbyteRead > 0))
    {
        if ((':' == revBuff[0]) 
         && ('\r' == revBuff[*nbyteRead - 2]) 
         && ('\n' == revBuff[*nbyteRead - 1]))
        {
            return true;
        }
    }
    return false;
}

/**
 *-----------------------------------------------------------------------------
 * @brief CAN 队列操作
 * 
 * @param cmd 
 * @param cmdLen 
 * @param revBuff 
 * @param buffLen 
 * @param nbyteRead 
 * @return boolean 
 *-----------------------------------------------------------------------------
 */
boolean CANbusQuery(char* cmd, int cmdLen, char* revBuff, int buffLen, int* nbyteRead)
{
    return CANbusSend(cmd, cmdLen) && CANbusRead(revBuff, buffLen, nbyteRead);
}