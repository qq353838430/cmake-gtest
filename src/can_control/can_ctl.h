/**
 *-----------------------------------------------------------------------------
 * @file can_ctl.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */
#ifndef _CAN_CTL_H_
#define _CAN_CTL_H_

#include "Rte_Type.h"

// Can Interface
extern boolean CANbusSend(char* cmd, int cmdLen);
extern boolean CANbusRead(char* revBuff, int buffLen, int* nbyteRead);
extern boolean CANbusQuery(char* cmd, int cmdLen, char* revBuff, int buffLen, int* nbyteRead);

// Serial Interface
extern boolean SerialWrite(void* data, int len, int* nbyteWrite);
extern boolean SerialRead(void* buff, int buffLen, int* nbyteRead);



#endif