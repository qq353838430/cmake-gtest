/**
 *-----------------------------------------------------------------------------
 * @file Rte_Type.h
 * @brief 
 * @author shibo jiang
 * @version 0.0.0.1
 * @date 2023-02-11
 * @note [change history] 
 * 
 * @copyright MULAN
 *-----------------------------------------------------------------------------
 */

#ifndef _RTE_TYPE_H
#define _RTE_TYPE_H
/* Include Files */
#include <stdio.h>
#include "test_type.h"

/* Macro define */
#define true 1U
#define false 0U

/* Type define */
typedef signed char int8_T;
typedef unsigned char uint8_T;
typedef short int16_T;
typedef unsigned short uint16_T;
typedef int int32_T;
typedef unsigned int uint32_T;
typedef float real32_T;
typedef double real64_T;
typedef double real_T;
typedef double time_T;
typedef unsigned char boolean_T;

/* AUTOSAR Base to Platform types mapping */
typedef boolean_T boolean;
typedef int16_T sint16;
typedef int32_T sint32;
typedef int8_T sint8;
typedef uint16_T uint16;
typedef uint32_T uint32;
typedef uint8_T uint8;
typedef real32_T float32;
typedef real_T float64;

#endif
